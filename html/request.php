<? require_once $_SERVER['DOCUMENT_ROOT'] . '/../framework/_base.php';
################################################################################
# /html/request.php
# all endpoint requests are routed through this file
# blimp

################################################################################
# urlconf
# kinda super stole this from django
# this config maps all user-requested endpoints to their respective scripts
# the key is a regex that the users request must match
# the value is the path to the script (minus php extension)
# its also possible to throw in a normal string with no key/value pair
# this will be run through an exact match and will call the corresponding
# script if the match is met
# (example: 'logout' would map 'https://site.com/logout' to 'endpoints/logout')
# matches are performed in the order presented in the urlconf

$urlconf = [

	# boring top-level links
	'index',
	'login',
	'logout',

	# anonymous contribution links
	'contribute/report-incident',

	# manage reports and incidents
	'volunteer/create-incident',
	'volunteer/tag-incident',
	'volunteer/delete-report',
	'volunteer/list-reports',
	'volunteer/view-report',

	# tag management TODO
	'volunteer/tags/manage',
	'volunteer/tags/create',
	'volunteer/tags/cat-create',
	'volunteer/tags/delete',

	# view incident ... for public and volunteer use
	'view-incident',

	# catch-all ... try to find a matching public incident (no slashes allowed)
	'/^([0-9]+)$/'        => 'view-incident', # id
	'/^([A-Za-z0-9-]+)$/' => 'view-incident', # slug

];

################################################################################
# process endpoint request

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# preprocessing

# trim leading and trailing slashes
$__requested_page = trim($_GET['__requested_page'], '/');

# special case for index
if (!$__requested_page) $__requested_page = 'index';

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# locate matching endpoint

foreach ($urlconf as $k => $endpoint) {

	# no key specified, perform exact match
	if (is_int($k)) {

		# found a match!
		# run requested script and exit
		if (!strcmp($endpoint, $__requested_page)) {
			require_once $_SERVER['DOCUMENT_ROOT'] . "/../endpoints/$endpoint.php";
			exit();
		}

	}

	# key specified, use key as regex
	else {

		# regex matches!
		if (preg_match($k, $__requested_page, $parameters)) {
			require_once $_SERVER['DOCUMENT_ROOT'] . "/../endpoints/$endpoint.php";
			exit();
		}

	}

}

# requested url didnt match anything we have
http_response_code(404);
die();
