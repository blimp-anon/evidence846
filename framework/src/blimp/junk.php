<? #############################################################################
# /framework/_blimp/junk.php
# you know how every house has a junk drawer full of things that you sometimes
# (or maybe even frequently) use, but that doesnt really have a home?
# this is that junk drawer.
# shit in here really should be better organized, but meh
# ill get around to it
# blimp

################################################################################
# uuid
# https://stackoverflow.com/a/15875555
function uuid4 () {

	$data = random_bytes(16);
    $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10
    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));

}

################################################################################
# base64url
# https://www.php.net/manual/en/function.base64-encode.php

function base64url_encode ($data) {
	return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
}

function base64url_decode ($data) {
	return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
}

################################################################################
# json_exit
# echo json data (usually used for ajax requests)
function json_exit ($data, $exitcode = 0) {

	# set content type
	header('Content-Type: application/json');

	# echo data
	echo json_encode($data);

	# and exit
	exit($exitcode);

}

################################################################################
# url_filter + url_validate
# safely process urls
# modified from https://stackoverflow.com/a/43746181

function url_filter ($url) {
	$path = parse_url($url, PHP_URL_PATH);
    $encoded_path = array_map('urlencode', explode('/', $path));
    return str_replace($path, implode('/', $encoded_path), $url);
}

function url_validate ($url) {
	$filtered = url_filter($url);
	return filter_var($filtered, FILTER_VALIDATE_URL) ? $filtered : false;
}

################################################################################
# sanitize
# allows for safe display of user-provided data
# this function is critical for security!!
# this isnt good enough for url sanitization, see the above url_validate
function sanitize ($data) {
	return htmlentities($data, ENT_QUOTES);
}

################################################################################
# die_response_code
# shorthand for echo response code and die
function die_response_code ($code) {
	http_response_code($code);
	die();
}

################################################################################
# verctrl_mostrecent_qstr
# query string to grab most recent version of whatever
# https://stackoverflow.com/questions/3800551
# https://dev.mysql.com/doc/refman/5.6/en/example-maximum-column-group-row.html
# solution to the greatest-n-per-group problem
# TODO clean this up! formalize/generalize parameters
function verctrl_mostrecent_qstr ($table, $pk, $whereclause = null, $ver = 'ver', $fields = '*') { return "
	SELECT $fields
		FROM $table t1
		WHERE $ver = (
			SELECT MAX(t2.$ver)
				FROM $table t2
				WHERE t1.$pk = t2.$pk
			)
			" . ($whereclause ? "AND $whereclause\n" : "\n")
; }

################################################################################
# css_tagcat_updatereplace
# i have a css file dedicated to styles for tags
# this function allows editing of that file
# call with a tcid and ['bg'=>'', 'fg'=>''] array
function css_tagcat_updatereplace ($tcid, $colors) {

	# lets abuse css specificity and just append the new rule to the css file
	# https://www.w3schools.com/css/css_specificity.asp

	# open file for appended editing
	$fp = fopen($_SERVER['DOCUMENT_ROOT'] . '/static/src/_global/_tags.css', 'a');
	if (!$fp) return false;

	# append the new line
	fwrite($fp, ".tag.tc$tcid { color: {$colors['fg']}; background-color: {$colors['bg']}; border-color: {$colors['bg']}; }\n");

	fclose($fp);

	return true;

}
