<?php ##########################################################################
# /framework/captcha.php
# generate and validate captchas
# thanks to https://textcaptcha.com
# blimp

################################################################################
# captcha_generate
# returns a textual captcha as an associative array
function captcha_generate () {

	# subject to change if textcaptcha decides to change it?
	$url = 'http://api.textcaptcha.com/example.json';

	# get the captcha from textcaptcha api
	$captcha = json_decode(file_get_contents($url), true);

	# set defaults if we couldnt get a captcha (blank question/answer)
	if (!$captcha)
		$captcha = [ 'q' => '', 'a' => [ md5('') ] ];

	# properly salt/hash the captcha answers
	# this will help protect against dictionary attacks
	foreach ($captcha['a'] as $k => $v)
		$captcha['a'][$k] = md5( $v . $GLOBALS['secret_captcha_salt'] );

	# captcha all set, return
	return $captcha;

}

################################################################################
# captcha_validate
# validate a captcha response against possible answers
function captcha_validate ($response, $answers) {

	# prepare response
	# textcaptcha takes answers and applies the trim->strtolower->md5 chain
	# lets reproduce that here ... then append salt and md5 again
	# like we did when generating the captcha
	$resp = md5( md5(strtolower(trim($response))) . $GLOBALS['secret_captcha_salt'] );

	# see if weve got an answer
	return in_array($resp, $answers);

}
