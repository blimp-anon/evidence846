<? #############################################################################
# /framework/auth.php
# handles authentication via magic links
# blimp

################################################################################
# auth_user
# grab a data object representing the currently logged-in user
function auth_user ($attr = null) {

	# if no user in session
	if (!array_key_exists('user', $_SESSION))
		return null;

	# attr set to null, just return the whole object
	if (!$attr)
		return $_SESSION['user'];

	# attr non-null, offer this as a shorthand
	return $_SESSION['user'][$attr];

}

################################################################################
# auth_perms
# check if a user has the required permission level
function auth_perms ($level) {

	# grab the current user
	$user = auth_user();

	# if nobodys logged in, deny
	if (!$user) return false;

	# check if permissions are sufficient
	# a lower permission number signifies a more elevated user
	# with 0 being superuser status
	return $user['perms'] <= $level;

}

################################################################################
# auth_authenticate
# attempt to log in a user
function auth_authenticate ($token) {

	# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
	# attempt to locate the user

	# make the token secure (like how its stored in the db)
	$token_secure = md5($token . $GLOBALS['secret_auth_salt']);

	# attempt to locate a magic link with this token
	$magic = DB::queryFirstRow('SELECT * FROM user_magic WHERE token = %s', $token_secure);

	# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
	# validation

	# return if no match
	if (!$magic)
		return false;

	# return if link has been used
	if (!$magic['new'])
		return false;

	# return if link is expired (10min)
	if ( (new DateTime())->getTimestamp()
		- (new DateTime($magic['ts_create']))->getTimestamp()
		> 60*10 )
			return false;

	# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
	# import user information

	# query db
	$_SESSION['user'] = DB::queryFirstRow('SELECT * FROM user_info WHERE email = %s', $magic['email']);

	# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
	# update magic link

	# all magic links for this user are now invalid
	DB::update('user_magic', ['new' => 0], 'email = %s', $magic['email']);

	# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
	# successfully authenticated

	return true;

}

################################################################################
# auth_logout
# log a user out
# https://www.php.net/manual/en/function.session-destroy
function auth_logout () {

	# TODO figure out something more secure maybe

	# unset all session variables
	$_SESSION = [];

	# expire session cookie
	if (ini_get('session.use_cookies')) {
		$params = session_get_cookie_params();
		setcookie(
			session_name(), '', 1,
			$params['path'], $params['domain'],
			$params['secure'], $params['httponly'] );
	}

	# destroy the session
	session_destroy();
	session_regenerate_id(true);

	# restart the session
	session_start();

}

################################################################################
# auth_sendmagiclink
# send a magic link to the users email address
function auth_sendmagiclink ($email) {

	# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
	# find the user

	# check if the email address is associated with a user
	$user = DB::queryFirstRow('SELECT * FROM user_info WHERE email = %s', $email);

	# abort if no user, of course
	if (!$user)
		return false;

	# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
	# generate token

	# actually create the token
	$token = base64url_encode( random_bytes(32) );

	# make it secure for db storage
	$token_secure = md5($token . $GLOBALS['secret_auth_salt']);

	# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
	# save credentials

	DB::insert('user_magic', [
		'email' => $user['email'],
		'token' => $token_secure,
	]);

	# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
	# send email

	$mail = new PHPMailer\PHPMailer\PHPMailer(true);

	try {

		$mail->isSMTP();
		$mail->Host       = $GLOBALS['secret_mail_host'];
		$mail->SMTPAuth   = true;
		$mail->Username   = $GLOBALS['secret_mail_user'];
		$mail->Password   = $GLOBALS['secret_mail_pass'];
		$mail->SMTPSecure = PHPMailer\PHPMailer\PHPMailer::ENCRYPTION_SMTPS;
		$mail->Port       = $GLOBALS['secret_mail_port'];

		$mail->setFrom($GLOBALS['secret_mail_from'], $GLOBALS['secret_mail_name']);
		$mail->addAddress($email);

		$mail->isHTML(true);
		$mail->Subject = '846evidence.com login link';
		$mail->Body = "<a href='https://846evidence.com/login?token=$token'>click here to login.</a><br><br>this link is valid for 10 minutes and cannot be reused.";

		$mail->send();

	} catch (PHPMailer\PHPMailer\Exception $e) {
		return false;
	}

	return true;

}

################################################################################
# auth_requireperm
# send a 403 error if the logged in user
# doesnt have the required perms to view this page
function auth_requireperm ($level) {

	# grab the current user
	$user = auth_user();

	# return if the user is properly authenticated and has the required perms
	if (auth_perms($level))
		return true;

	# either nobody is logged in,
	# or the logged-in user doesnt have permission to view this page
	# send a 403 error
	http_response_code(403);

	# and stop script execution here
	die();

}
