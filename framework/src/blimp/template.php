<? #############################################################################
# /framework/template.php
# handles authentication via magic links
# blimp

################################################################################
# template_render
# print the html for the requested page
function template_render ($vars = null) {

	# re-using from /html/request.php
	global $endpoint;

	# prepare mthaml
	$mthaml = new MtHaml\Support\Php\Executor(
		new MtHaml\Environment('php'),
		[ 'cache' => $_SERVER['DOCUMENT_ROOT'] . '/../cache/haml' ]
	);

	# use empty array if no vars
	$vars = $vars ?? [];

	# and add in the endpoint
	$vars['endpoint'] = $endpoint;

	# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
	# render template

	#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
	# head

	echo '<html><head>';

	# load common head
	$mthaml->display( $_SERVER['DOCUMENT_ROOT'] . "/../src/_global/.head.haml", $vars );

	# load template head
	if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/../src/$endpoint.head.haml"))
		$mthaml->display( $_SERVER['DOCUMENT_ROOT'] . "/../src/$endpoint.head.haml", $vars );

	#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
	# body

	echo '</head><body>';

	# load common body
	$mthaml->display( $_SERVER['DOCUMENT_ROOT'] . "/../src/_global/.body.haml", $vars );

	# surround template body in main tags
	echo '<main>';

	# load template body
	if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/../src/$endpoint.body.haml"))
		$mthaml->display( $_SERVER['DOCUMENT_ROOT'] . "/../src/$endpoint.body.haml", $vars );

	echo '</main></body></html>';

}

################################################################################
# template_var
# grab a variable sent to the template, default to null
function template_var ($k) {
	return array_key_exists($k, $GLOBALS['TEMPLATE'])
		? $GLOBALS['TEMPLATE'][$k] : null;
}
