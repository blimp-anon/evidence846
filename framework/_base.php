<? #############################################################################
# /framework/_base.php
# loaded in by literally every single page
# handles loading in all frameworks and dependencies
# blimp

################################################################################
# secret variables

# these variables represent secret values like passwords and keys
# they should not be shared under any circumstances
# instead, i have documentation for each of these below:

# $secret_db_user
# $secret_db_pass
# $secret_db_name
# username, password, and database name for connection to our database
# if these are compromised, an attacker will have full access to our database
# and can overwrite, delete, or otherwise sabotage at will
# THESE SHOULD REMAIN PRIVATE AT ALL COSTS

# $secret_mail_host
# $secret_mail_port
# $secret_mail_user
# $secret_mail_pass
# $secret_mail_from
# $secret_mail_name
# phpmailer settings.
# host and port are to connect to the smtp server.
# user and pass authenticate.
# from is the email address mail is sent from, name is the name of that account
# if these are compromised, an attacker can send email as the server...

# $secret_auth_salt
# salt for magic link authentication requests
# if this is compromised, our login system is weakened until it is changed

# $secret_captcha_salt
# salt for generating captchas
# if this is compromised, bots can break our captchas until it is changed

# do not share this
# dont even look at it
# i cut you
require_once $_SERVER['DOCUMENT_ROOT'] . '/../secret/_base.php';

################################################################################
# first-party shit

# my stuff
require_once 'src/blimp/_base.php';

################################################################################
# third-party

# meekrodb
# database access
require_once 'lib/meekrodb-2.3/db.class.php';
DB::$user =     $secret_db_user;
DB::$password = $secret_db_pass;
DB::$dbName =   $secret_db_name;

# phpmailer
require_once 'lib/PHPMailer-master/src/Exception.php';
require_once 'lib/PHPMailer-master/src/PHPMailer.php';
require_once 'lib/PHPMailer-master/src/SMTP.php';

# mthaml
# convert haml files to php
require_once 'lib/MtHaml/Autoloader.php';
MtHaml\Autoloader::register();

################################################################################
# miscellaneous

# start the session now
session_start();
