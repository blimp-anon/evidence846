$ ->

	$('main form input[name=name], main form select[name=tcid]').on 'change update keyup', update_tag_preview

	$('main form input[name=name]').on 'change update keyup', update_slug

	update_tag_preview()

update_tag_preview = () ->
	$('main form .tag')
		.text $('main form input[name=name]').val()
		.removeClass()
		.addClass ['tag', 'tc' + $('main form [name=tcid]').val().toString()]

update_slug = () ->
	$('main form input[name=slug]').val(
		$('main form input[name=name]').val().toLowerCase()
			.replace /\s+/g, '-'
			.replace /[^a-z0-9-]/g, ''
		)
