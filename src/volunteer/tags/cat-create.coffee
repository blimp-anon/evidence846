$ ->

	$('main form input[type=color]').on 'change', update_tag_preview

	update_tag_preview()

update_tag_preview = () ->
	$('main form .tag').css {
		'color':        $('main form [name=color_fg]').val()
		'background':   $('main form [name=color_bg]').val()
		'border-color': $('main form [name=color_bg]').val()
	}
