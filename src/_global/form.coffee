################################################################################
# form.coffee
# form submission logic
# blimp

################################################################################
$ ->

	$('form').submit form_handlesubmit

################################################################################
# form_handlesubmit
# process a form submission
# lets use ajax here cause its cool
# doing this also allows us to call hooks for before and after submission
# you can use something like <form data-hook-before-submit="mybeforefunc" ...>
# to have this script call mybeforefunc(form_element)
# and then optionally return false to prevent form submission
# or use something like <form data-hook-after-submit="myafterfunc" ...>
# to have this script call myafterfunc(response_object)
# once the server has responded
window.form_handlesubmit = (e) ->

	# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
	# init

	# prevents normal boring form submission
	e.preventDefault()

	# call the beforesubmit hook
	# this hook can return false to interrupt the form submission
	# maybe it hasnt been validated properly or something
	if false == call_func e.target.dataset.hookBeforeSubmit, e.target
		return false

	# find the forms submit button...
	btn = $(e.target)
		.find '[type="submit"]'

	# ... and disable it to prevent multiple submissions
	# (dont worry, ill enable it again later)
	btn.prop 'disabled', true

	# get data object to be submitted
	# https://developer.mozilla.org/en-US/docs/Web/API/FormData/Using_FormData_Objects
	# this is to help with submitting images
	formdata = new FormData e.target

	console.log 'submitting form:'
	console.log formdata # TODO remove maybe

	# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
	# submit ajax request

	nowres = $.ajax e.target.action, {

		data: formdata
		dataType: 'json'
		method: e.target.method

		# https://stackoverflow.com/questions/2320069/jquery-ajax-file-upload
		# apparently jquery autoprocesses this section for us
		# lets not have them do that since were using raw formdata
		processData: false
		contentType: false

		# callback
		complete: (finalres, status) ->

			# lets not forget the submit button is still disabled
			btn.prop 'disabled', false

			# lets do some generic processing
			form_aftersubmit finalres

			# submit the response to the appropriate callback
			call_func e.target.dataset.hookAfterSubmit, finalres

			# execute a redirect if instructed
			if finalres.status == 200 && finalres.responseJSON && finalres.responseJSON.redirect
				window.location.href = finalres.responseJSON.redirect

			# or a reload if instructed
			if finalres.status == 200 && finalres.responseJSON && finalres.responseJSON.reload
				window.location.reload()

	}

	false

################################################################################
# form_aftersubmit
# generic processing of responses
window.form_aftersubmit = (res) ->

	# TODO maybe something more useful
	if res.responseJSON.msg
		alert res.responseJSON.msg
