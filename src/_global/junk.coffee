################################################################################
# junk.coffee
# junk drawer for javascript
# blimp

################################################################################
# call_func
# safe way to call a function regardless of if its a string or what
# blimp
window.call_func = (f, ...argv) ->

	# easy mode
	# call normally if passed a function
	# pass f as first argument to apply so it has itself as this
	if typeof(f) == 'function'
		return f.apply f, argv

	# passed a string
	if typeof(f) == 'string'

		# empty string ... return undefined
		if !f
			return

		# we found the string as a global function
		if window[f]
			return window[f].apply window[f], argv

		# none of these things are true
		# we couldnt find a function to call
		return

	# passed something else
	# this is weird ...
	return

################################################################################
# preview_image
# show image in browser when user selects it from file select
# http://jsbin.com/uboqu3/1/edit?html,js,output
# blimp
window.preview_image = (input, previewid, defaultsrc='') ->

	if input.files && input.files[0]

		reader = new FileReader()
		reader.onload = (e) ->
			document.getElementById(previewid).src = e.target.result
		reader.readAsDataURL input.files[0]

	else document.getElementById(previewid).src = defaultsrc
