-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 21, 2020 at 05:09 PM
-- Server version: 8.0.20-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `pb`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_final_events`
--

CREATE TABLE `data_final_events` (
  `feid` int NOT NULL COMMENT '(pk) final event id',
  `ver` int NOT NULL DEFAULT '0' COMMENT 'version. allows us to keep track of edits and roll back if necessary. starts at 1 and goes up from there.',
  `uid` int NOT NULL COMMENT 'uid of the user who created this version of this incident',
  `ts_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ts of creation of this particular version, not necessarily of the incident as a whole',
  `slug` text COLLATE utf8_bin NOT NULL COMMENT 'the incident url. must only contain lowercase english letters, numbers, and the hyphen (-). make this short yet descriptive to make it easy to remember, type, and share.',
  `title` text COLLATE utf8_bin NOT NULL COMMENT 'the indicent title. keep this short yet descriptive.',
  `thumbnail` tinytext CHARACTER SET utf8 COLLATE utf8_bin COMMENT 'path to this incidents thumbnail.',
  `description` text COLLATE utf8_bin NOT NULL COMMENT 'anon should be able to read this description in lieu of watching sources. should fully inform anon while refraining from the use of bias or loaded language.',
  `date` date NOT NULL,
  `loc_city` tinytext COLLATE utf8_bin NOT NULL,
  `loc_state` varchar(2) COLLATE utf8_bin NOT NULL COMMENT 'two character state code',
  `importance` int NOT NULL COMMENT 'currently only used to determine if this event can show up on the front page, might be repurposed later. 0=no front page, 1=front page qualified',
  `delete_reason` text COLLATE utf8_bin COMMENT 'if not null, this entire incident should be considered deleted. the user responsible for deletion (and this version) will write their reasoning here.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='stores curated incident information for public perusal';

--
-- Triggers `data_final_events`
--
DELIMITER $$
CREATE TRIGGER `auto update version and slug` BEFORE INSERT ON `data_final_events` FOR EACH ROW SET
	new.ver = 1 + (
    	SELECT IFNULL(MAX(ver), 0)
			FROM data_final_events
    		WHERE feid = new.feid
    	),
    new.slug = IFNULL(new.slug, (
        SELECT slug
        	FROM data_final_events
        	WHERE feid = new.feid
        		AND ver = 1 ) )
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `data_final_sources`
--

CREATE TABLE `data_final_sources` (
  `fsid` int NOT NULL COMMENT '(pk) final source id',
  `ver` int NOT NULL DEFAULT '0' COMMENT 'source version',
  `feid` int NOT NULL COMMENT 'associated incident',
  `uid` int NOT NULL COMMENT 'user id who created this source',
  `ts_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` tinytext COLLATE utf8_bin NOT NULL COMMENT 'one of: url | torrent',
  `title` text COLLATE utf8_bin COMMENT '(optional) title for the source. this is what the user clicks on to visit/download the source.',
  `value` text COLLATE utf8_bin NOT NULL COMMENT 'if type=url: full link to source hosted externally. if type=torrent: path to torrent file hosted locally.',
  `medium` tinytext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'one of: video | image | article | audio',
  `active` tinytext CHARACTER SET utf8 COLLATE utf8_bin COMMENT 'one of: NULL (represents an active link) | deleted | broken | irrelevant'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='finalized incident sources, to be shown to the public.';

--
-- Triggers `data_final_sources`
--
DELIMITER $$
CREATE TRIGGER `auto_update_ver` BEFORE INSERT ON `data_final_sources` FOR EACH ROW SET new.ver = 1 + (
    SELECT IFNULL(MAX(ver), 0)
		FROM data_final_sources
    	WHERE fsid = new.fsid
    )
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `data_raw_events`
--

CREATE TABLE `data_raw_events` (
  `reid` int NOT NULL COMMENT '(pk) raw event id',
  `ts_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` text COLLATE utf8_bin NOT NULL COMMENT 'succinct description',
  `thumbnail` text COLLATE utf8_bin COMMENT 'path to thumbnail image, if uploaded',
  `description` text CHARACTER SET utf8 COLLATE utf8_bin COMMENT 'complete description',
  `date` date NOT NULL,
  `loc_state` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'state code',
  `loc_city` text COLLATE utf8_bin NOT NULL,
  `action_uid` int DEFAULT NULL COMMENT 'the uid of the user who took action on this report',
  `action_taken` tinytext CHARACTER SET utf8 COLLATE utf8_bin COMMENT 'the action taken to process the report. one of: promote | delete',
  `action_feid` int DEFAULT NULL COMMENT 'linked feid. used if action_taken is one of: promote'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='raw user-submitted events, to be processed into final events';

-- --------------------------------------------------------

--
-- Table structure for table `data_raw_sources`
--

CREATE TABLE `data_raw_sources` (
  `rsid` int NOT NULL COMMENT '(pk) raw source id',
  `reid` int DEFAULT NULL COMMENT 'associated raw event id, null if for final event',
  `feid` int DEFAULT NULL COMMENT 'associated final event id, null if for raw event',
  `ts_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` tinytext COLLATE utf8_bin NOT NULL COMMENT 'url | torrent',
  `title` text COLLATE utf8_bin COMMENT 'source description (link text)',
  `value` text COLLATE utf8_bin NOT NULL COMMENT 'if type=url: link to evidence. if type=torrent: path to torrent file for evidence',
  `action_uid` int DEFAULT NULL COMMENT 'uid of user who handled this raw source',
  `action_taken` tinytext COLLATE utf8_bin COMMENT 'one of: promote | delete',
  `action_fsid` int DEFAULT NULL COMMENT 'if action_taken=promote, the fsid this was promoted to'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='raw user-submitted sources, can be for raw or final events';

-- --------------------------------------------------------

--
-- Table structure for table `data_tags_assoc`
--

CREATE TABLE `data_tags_assoc` (
  `taid` int NOT NULL COMMENT 'tag assoc id (not really used)',
  `uid` int NOT NULL COMMENT 'user who created this association',
  `tid` int NOT NULL COMMENT 'attach this tag ...',
  `feid` int NOT NULL COMMENT '... to this incident',
  `delete_uid` int DEFAULT NULL COMMENT 'user who deleted this association'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='associate tags with incidents';

-- --------------------------------------------------------

--
-- Table structure for table `data_tags_cats`
--

CREATE TABLE `data_tags_cats` (
  `tcid` int NOT NULL COMMENT 'tag category id',
  `ver` int NOT NULL COMMENT 'version control',
  `uid` int NOT NULL COMMENT 'user responsible for this version',
  `title` tinytext COLLATE utf8_bin NOT NULL COMMENT 'short title to be easily displayed with other tags/categories',
  `description` text COLLATE utf8_bin COMMENT 'longer description to be used on category search pages',
  `color_fg` varchar(7) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '#000000' COMMENT 'color to use for background of tags with this category',
  `color_bg` varchar(7) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '#CCCCCC' COMMENT 'foreground / text color',
  `discarded` text COLLATE utf8_bin COMMENT 'if not null, the reason why this category was discarded'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='info for larger categories that tags can fit into';

--
-- Triggers `data_tags_cats`
--
DELIMITER $$
CREATE TRIGGER `data_tags_cats_autover` BEFORE INSERT ON `data_tags_cats` FOR EACH ROW SET new.ver = 1 + (
    SELECT IFNULL(MAX(ver), 0)
		FROM data_tags_cats
    	WHERE tcid = new.tcid
    )
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `data_tags_info`
--

CREATE TABLE `data_tags_info` (
  `tid` int NOT NULL COMMENT 'tag id',
  `ver` int NOT NULL COMMENT 'version control',
  `uid` int NOT NULL COMMENT 'user responsible for this version',
  `tcid` int DEFAULT NULL COMMENT 'the category this tag belongs to, leave blank for no category (bad form)',
  `name` tinytext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'short name to be displayed alongside other tags in the tag section',
  `slug` text COLLATE utf8_bin NOT NULL COMMENT 'slug to be used in the url',
  `description` text CHARACTER SET utf8 COLLATE utf8_bin COMMENT 'longer description to be used in search pages etc',
  `discarded` text COLLATE utf8_bin COMMENT 'if not null, the reason this tag shouldnt be used anymore'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='metadata for incident tags';

--
-- Triggers `data_tags_info`
--
DELIMITER $$
CREATE TRIGGER `data_tags_info_autover` BEFORE INSERT ON `data_tags_info` FOR EACH ROW SET
	new.ver = 1 + (
    	SELECT IFNULL(MAX(ver), 0)
			FROM data_tags_info
    		WHERE tid = new.tid
    	),
    new.slug = IFNULL(new.slug, (
        SELECT slug
        	FROM data_tags_info
        	WHERE tid = new.tid
        		AND ver = 1 ) )
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `uid` int NOT NULL COMMENT 'user id',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `nickname` text COLLATE utf8_bin,
  `perms` int NOT NULL COMMENT '0=admin, all perms. 1=curator, can access data thru website and process submitted data'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='information for users.';

-- --------------------------------------------------------

--
-- Table structure for table `user_magic`
--

CREATE TABLE `user_magic` (
  `id` int NOT NULL COMMENT 'not used',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `token` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `new` int NOT NULL DEFAULT '1' COMMENT 'set to 0 when the link is clicked.',
  `ts_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='storage for magic link tokens';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_final_events`
--
ALTER TABLE `data_final_events`
  ADD PRIMARY KEY (`feid`,`ver`);

--
-- Indexes for table `data_final_sources`
--
ALTER TABLE `data_final_sources`
  ADD PRIMARY KEY (`fsid`,`ver`) USING BTREE;

--
-- Indexes for table `data_raw_events`
--
ALTER TABLE `data_raw_events`
  ADD PRIMARY KEY (`reid`);

--
-- Indexes for table `data_raw_sources`
--
ALTER TABLE `data_raw_sources`
  ADD PRIMARY KEY (`rsid`);

--
-- Indexes for table `data_tags_assoc`
--
ALTER TABLE `data_tags_assoc`
  ADD PRIMARY KEY (`taid`);

--
-- Indexes for table `data_tags_cats`
--
ALTER TABLE `data_tags_cats`
  ADD PRIMARY KEY (`tcid`,`ver`);

--
-- Indexes for table `data_tags_info`
--
ALTER TABLE `data_tags_info`
  ADD PRIMARY KEY (`tid`,`ver`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`uid`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `user_magic`
--
ALTER TABLE `user_magic`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_final_events`
--
ALTER TABLE `data_final_events`
  MODIFY `feid` int NOT NULL AUTO_INCREMENT COMMENT '(pk) final event id';

--
-- AUTO_INCREMENT for table `data_final_sources`
--
ALTER TABLE `data_final_sources`
  MODIFY `fsid` int NOT NULL AUTO_INCREMENT COMMENT '(pk) final source id';

--
-- AUTO_INCREMENT for table `data_raw_events`
--
ALTER TABLE `data_raw_events`
  MODIFY `reid` int NOT NULL AUTO_INCREMENT COMMENT '(pk) raw event id';

--
-- AUTO_INCREMENT for table `data_raw_sources`
--
ALTER TABLE `data_raw_sources`
  MODIFY `rsid` int NOT NULL AUTO_INCREMENT COMMENT '(pk) raw source id';

--
-- AUTO_INCREMENT for table `data_tags_assoc`
--
ALTER TABLE `data_tags_assoc`
  MODIFY `taid` int NOT NULL AUTO_INCREMENT COMMENT 'tag assoc id (not really used)';

--
-- AUTO_INCREMENT for table `data_tags_cats`
--
ALTER TABLE `data_tags_cats`
  MODIFY `tcid` int NOT NULL AUTO_INCREMENT COMMENT 'tag category id';

--
-- AUTO_INCREMENT for table `data_tags_info`
--
ALTER TABLE `data_tags_info`
  MODIFY `tid` int NOT NULL AUTO_INCREMENT COMMENT 'tag id';

--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `uid` int NOT NULL AUTO_INCREMENT COMMENT 'user id';

--
-- AUTO_INCREMENT for table `user_magic`
--
ALTER TABLE `user_magic`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'not used';
COMMIT;
