<? #############################################################################
# /endpointsview-incident.php
# display a finalized incident to the viewer
# blimp

################################################################################
# determine flow and parameters

# we were passed a reid as a query string parameter
# a volunteer is attempting to access a raw incident report
if (array_key_exists('reid', $_GET)) {
	auth_requireperm(1); # viewing raw report, restrict to volunteers
	$raworfinal = 'raw';
	$id = intval($_GET['reid']);
	$slug = null;
}

# we were passed a feid as a query string parameter
# anon is trying to view a public incident
else if (array_key_exists('feid', $_GET)) {
	$raworfinal = 'final';
	$id = intval($_GET['feid']);
	$slug = null;
}

# passed no query string parameters
# lets hope we are handling the catchall set up in the urlconf
else if (isset($parameters) && isset($parameters[1])) {

	# parameter passed is an int
	# search by feid
	if (is_numeric($parameters[1])) {
		$raworfinal = 'final';
		$id = intval($parameters[1]);
		$slug = null;
	}

	# we were passed a string (slug)
	else {
		$raworfinal = 'final';
		$id = null;
		$slug = strtolower(strval($parameters[1]));
	}

}

# we dont know what tf we were passed
else die_response_code(404);

################################################################################
# locate public information

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# locate incident

# select incident from database
# final+slug: SELECT * FROM data_final_events WHERE slug = %s ...
# final+id:   SELECT * FROM data_final_events WHERE feid = %i ...
# raw+id:     SELECT * FROM data_raw_events   WHERE reid = %i ...
# then pass the appropriate parameter for %s/%i
# i sort by version desc in final search to retrieve most recent edit
$incident = DB::queryFirstRow(

	# select the correct table
	"SELECT * FROM data_{$raworfinal}_events WHERE "

	# choose the correct field and parameter type
	. ($slug ? 'slug = %s' : ($raworfinal == 'raw' ? 'reid = %i' : 'feid = %i'))

	# select most recent version of final entry
	. ($raworfinal == 'raw' ? '' : ' ORDER BY ver DESC') ,

	# pass the correct parameter
	($slug ? $slug : $id)

);

# couldnt find the incident, or incident has been deleted
if (!$incident || $incident['delete_reason'])
	die_response_code(404);

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# locate sources

# this time i separate into two distinct queries

# raw report
if ($raworfinal == 'raw')
	$sources = DB::query("SELECT * FROM data_raw_sources WHERE reid = %i", intval($incident['reid']));

# final incident
# the below function helps alot with version control
else $sources = DB::query( verctrl_mostrecent_qstr('data_final_sources', 'fsid', 'feid = %i'), intval($incident['feid']));
# this is alot more complicated
# final sources are versioned, which allows us to track their history
# and roll back mistakes easily ...
# but it also means we cant just waltz in and select all sources matching feid
# because wed be selecting all older-version sources too
# so lets create a temp table with a new column rownum
# partition / order correctly such that rownum=1 if weve got the newest version
# then only select the rownum=1 from the temp table
# https://stackoverflow.com/questions/3800551
/*else $sources = DB::query("
		WITH niceandneat AS (
			SELECT
				srcs.*,
				ROW_NUMBER()
					OVER (
						PARTITION BY srcs.fsid
						ORDER BY srcs.ver DESC )
					AS rownum
			FROM data_final_sources srcs
			WHERE srcs.feid = %i )
		SELECT n.*
		FROM niceandneat n
		WHERE n.rownum = 1
	", intval($incident['feid']) );*/

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# locate tags

# grab all tids linked to this incident
#   (but only if the most revent version of that linkage is valid)
# then grab tag informtation for those tids
#   (but only if those tags havent been deleted)
# the nested function calls below make this look much more compact than it
# actually is, here's an example:
#
#	WITH mytmp AS (
#		SELECT
#			mytbl.*,
#			ROW_NUMBER()
#				OVER (
#					PARTITION BY mytbl.tid
#					ORDER BY mytbl.ver DESC )
#				AS rownum
#			FROM data_tags_info mytbl
#			WHERE
#				tid IN (
#					WITH mytmp AS (
#						SELECT
#							mytbl.*,
#							ROW_NUMBER()
#								OVER (
#									PARTITION BY mytbl.tid
#									ORDER BY mytbl.taid DESC )
#								AS rownum
#							FROM data_tags_assoc mytbl
#								WHERE feid = %i
#						)
#					SELECT tid
#						FROM mytmp
#						WHERE rownum = 1
#							AND delete_uid IS NULL
#					)
#		)
#	SELECT *
#		FROM mytmp
#		WHERE rownum = 1
#			AND discarded7 IS NULL
#
# ... ugh

$tags = DB::query(verctrl_mostrecent_qstr('data_tags_info', 'tid', "
	tid IN ( " . verctrl_mostrecent_qstr('data_tags_assoc', 'tid', 'feid = %i', 'taid', 'tid')
	. "AND delete_uid IS NULL) " ) . "AND discarded IS NULL", intval($incident['feid']));

################################################################################
# retrieve private tagging information

# only access this section if were able to modify tags for a public incident
if ($raworfinal == 'final' && auth_perms(1)) {

	# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
	# retrieve all tags and tag cats

	$alltags = DB::query( verctrl_mostrecent_qstr('data_tags_info', 'tid')
 		. "AND discarded IS NULL ORDER BY tcid DESC, tid DESC");

}

################################################################################
# render template

template_render([
	'incident' => $incident,
	'sources' => $sources ?? [],
	'tags' => $tags,
	'raworfinal' => $raworfinal,
	'raw' => $raworfinal == 'raw',
	'final' => $raworfinal == 'final',
	'alltags' => $alltags,
	'alcats' => $allcats,
]);
