<? #############################################################################
# /endpoints/volunteer/create-incident.php
# make a new publicly-visible incident
# can optionally use an existing anon-submitted report as a template
# blimp

auth_requireperm(1);

################################################################################
# process post request

if ($_POST) {

	do {

		# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
		# validate fields

		$validated = [];
		$validated_sources = [];
		$validated_rsids = [];

		$reid = isset($_POST['reid']) ? intval($_POST['reid']) : null;
		$feid = isset($_POST['feid']) ? intval($_POST['feid']) : null;
#		if ($feid) { $err = json_encode($_POST); break; }

		#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
		# sources

		# loop over each submitted source
		foreach ($_POST['sources_i'] as $i) {

			# initialization
			$src_value  = $_POST['sources_value'][$i];
			$src_medium = $_POST['sources_medium'][$i];
			$src_title  = $_POST['sources_title'][$i];
			$src_rsid   = $_POST['sources_rsid'][$i]; # null if no rsid
			$src_fsid   = $_POST['sources_fsid'][$i]; # null if no fsid

			# make sure we have something to insert
			# we need at minimum a value and a medium
			if (!$src_value || !$src_medium)
				continue;

			# check for invalid source types
			if (!in_array(strtolower($src_medium), ['video','image','article','audio']))
				continue;

			# validate the link
			$url = url_validate($src_value);

			# next entry if not valid
			if ($url === false)
				continue;

			# aight we are good
			$validated_sources[] = [

				# will fill out the feid below

				# easy stuff
				# no need to sanitize medium, we did an in_array above
				'uid' => auth_user()['uid'],
				'type' => 'url',
				'value' => $url,
				'medium' => strtolower($src_medium),

				# use a title if present
				# otherwise fall back onto the url
				'title' => ( $src_title ? $src_title : $src_val ),

				# source versioning
				'fsid' => $src_fsid ? $src_fsid : null,

			];

			# also record the raw source id for easier processing later
			if ($reid)
				$validated_rsids[] = $src_rsid;

		}

		#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
		# importance

		# for now, just set to 1 if we get anything at all
		$validated['importance'] = isset($_POST['importance']) && $_POST['importance'] ? 1 : 0;

		#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
		# date

		# check for properly formatted dates
		# this should be the default for <input type='date'> but it cant hurt
		if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $_POST['date'])) {
			$err = 'malformed date entered. please use a date of the form yyyy-mm-dd.';
			break;
		}

		$validated['date'] = $_POST['date'];

		#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
		# loc_state

		# exactly two characters representing state code
		if (!preg_match('/^[A-Z]{2}$/', strtoupper($_POST['loc_state']))) {
			$err = 'please enter a two-character state code';
			break;
		}

		$validated['loc_state'] = strtoupper($_POST['loc_state']);

		#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
		# slug

		# so i lied
		# capital letters are fine
		# i convert them to lowercase anyways
		# also i only perform slug checks for new incidents...
		# edits ignore it and use a sql trigger to keep the slug consistent
		if ($reid) {

			# if the slug contains any illegal characters ... return to sender
			if (preg_match('/[^0-9A-Za-z-]/', $_POST['slug'])) {
				$err = 'invalid slug entered. please refrain from using any character thats not an english letter, number, or hyphen -';
				break;
			}

			# return if slug is already in use
			if (DB::queryFirstField('SELECT slug FROM data_final_events WHERE slug = %s', strtolower($_POST['slug']))) {
				$err = 'chosen slug is already in use, please try another.';
				break;
			}

		}

		# passed validation
		$validated['slug'] = strtolower($_POST['slug']);

		#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
		# thumbnail

		# if we have a thumbnail
		if (isset($_FILES['thumbnail']) && $_FILES['thumbnail']['size']) {

			# get the mime type of uploaded file
			$thumbnail_mime = mime_content_type($_FILES["thumbnail"]["tmp_name"]);

			# abort if we dont have an image
			if (!preg_match('/^image\/(png|jpeg|bmp|gif)$/', $thumbnail_mime)) {
				var_dump($thumbnail_mime);
				$err = 'invalid file uploaded, please select a png, gif, jpg, or bmp image.';
				break;
			}

			# abort if image is too large
			if ($_FILES['thumbnail']['size'] > 10000000) {
				$err = 'image is too large. please keep it under 10MB.';
				break;
			}

			# determine new file name
			$validated['thumbnail'] = uuid4() . '.png';

			# convert and save image
			imagepng(
				imagecreatefromstring(file_get_contents($_FILES['thumbnail']['tmp_name'])),
				$_SERVER['DOCUMENT_ROOT'] . '/uploads/images/' . $validated['thumbnail']
			);

		}

		# we dont have a thumbnail
		# if the creator used an incident report as a template,
		# assume they wanted to use the template-provided thumbnail
		else if (isset($_POST['thumbnail_default']) && $_POST['thumbnail_default'])
			$validated['thumbnail'] = $_POST['thumbnail_default'];

		# no thumbnail uploaded i guess
		else $validated['thumbnail'] = null;

		#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
		# prepare new version of existing report

		# just set the feid, triggers will handle everything else
		if ($feid) $validated['feid'] = $feid;

		#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
		# everything else

		$validated['title'] = ($_POST['title']);
		$validated['description'] = ($_POST['description']);
		$validated['loc_city'] = ($_POST['loc_city']);
		$validated['uid'] = auth_user()['uid'];

		# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
		# perform db insertions

		#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
		# final events

		# insert event into events table
		DB::insert('data_final_events', $validated);

		# get the id for that insertion
		$feid = DB::insertId();

		#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
		# final sources

		# update all sources with this id
		for ($i = 0; $i < count($validated_sources); $i++)
			$validated_sources[$i]['feid'] = $feid;

		# and insert sources
		DB::insert('data_final_sources', $validated_sources);

		# generate an array of fsids generated by the above insert
		# meekrodbs builtin insertId (taken from mysqli) doesnt cut it here
		# instead query LAST_INSERT_ID() directly and use that to build a range
		$first_fsid = DB::queryFirstField('SELECT LAST_INSERT_ID()');
		$fsids = range( $first_fsid, $first_fsid + count($validated_sources) - 1 );

		#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
		# updates for raw sources/events

		if ($reid) {

			# map old source ids to new ones
			# https://stackoverflow.com/questions/25674737
			# https://dev.mysql.com/doc/refman/5.7/en/case.html
			# use the case keyword liberally
			# if the rsid is marked as validated, then mark it as promoted
			# else mark it as deleted
			# then for all promoted rsids, use autogenerated case statements
			# to fill in the action_fsid field with the fsid it was promoted to
			$qs = "
				UPDATE data_raw_sources
					SET action_uid = " . strval(auth_user()['uid']) . ",
						action_taken = CASE
							WHEN rsid IN (" . implode(',', $validated_rsids) . ") THEN 'promote'
							ELSE 'delete'
							END,
						action_fsid = CASE\n";
							foreach($validated_rsids as $i => $qrsid)
								$qs .= "\t\t\t\t\t\t\tWHEN rsid = $qrsid THEN {$fsids[$i]}\n";
							$qs .= "\t\t\t\t\t\t\tELSE NULL
						END
					WHERE reid = $reid";
			DB::query($qs);

			# mark raw event as processed and note the final event id
			DB::update('data_raw_events', [
				'action_uid' => auth_user()['uid'],
				'action_taken' => 'promote',
				'action_feid' => $feid,
			], 'reid = %i', $reid);

		}

	} while (0);

	# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
	# cleanup

	json_exit([
		'err' => isset($err) ? 1 : 0,
		'msg' => $err ?? 'the incident has been created and is now publicly '
			. 'visible and shareable.',
		'redirect' => (isset($err) && $err) ? '' : '/' . ($validated['slug'] ? $validated['slug'] : strval($validated['feid'])),
	]);

}

################################################################################
# process get request

$vars = [];

if ($_GET) {

	# compile template from a report
	if (array_key_exists('reid', $_GET)) {

		$vars['raw'] = true;
		$vars['final'] = false;

		$vars['report'] = DB::queryFirstRow('SELECT * FROM data_raw_events WHERE reid = %i', intval($_GET['reid']));

		$vars['sources'] = DB::query("SELECT * FROM data_raw_sources WHERE reid = %i AND action_uid IS NULL ORDER BY rsid ASC", intval($_GET['reid'])) ?? [];

		# suggest a slug
		$vars['report']['slug'] = preg_replace('/[^0-9a-z-]/', '',
			preg_replace('/\s+/', '-',
				strtolower($vars['report']['title']
		)));

	}

	# compile template from existing final incident
	else if (array_key_exists('feid', $_GET)) {

		$vars['raw'] = false;
		$vars['final'] = true;

		# select most recent version of event
		$vars['report'] = DB::queryFirstRow(
			'SELECT * FROM data_final_events WHERE feid = %i ORDER BY ver DESC',
			intval($_GET['feid'] ));

		# select most revent version of each source
		$vars['sources'] = db::query("
				WITH niceandneat AS (
					SELECT
						srcs.*,
						ROW_NUMBER()
							OVER (
								PARTITION BY srcs.fsid
								ORDER BY srcs.ver DESC )
							AS rownum
					FROM data_final_sources srcs
					WHERE srcs.feid = %i )
				SELECT n.*
				FROM niceandneat n
				WHERE n.rownum = 1
			", intval($_GET['feid']) );

	}

}

template_render($vars);
