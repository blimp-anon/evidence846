<? #############################################################################
# /endpoints/volunteer/tags/create.php
# create a new tag (or update existing)
# blimp

auth_requireperm(1);

################################################################################
# handle post request

if ($_POST) {

	do {

		# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
		# validation

		$validated = [];

		#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
		# slug

		# only validate if creating a new tag
		if (!$_POST['tid']) {

			# if the slug contains any illegal characters ... return to sender
			if (preg_match('/[^0-9A-Za-z-]/', $_POST['slug'])) {
				$err = 'invalid slug entered. please refrain from using any character thats not an english letter, number, or hyphen -';
				break;
			}

			# return if slug is already in use
			if (DB::queryFirstField('SELECT slug FROM data_tags_info WHERE slug = %s', strtolower($_POST['slug']))) {
				$err = 'chosen slug is already in use, please try another.';
				break;
			}

			$validated['slug'] = strtolower($_POST['slug']);

		}

		#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
		# tag id (for new version of existing tag)

		if ($_POST['tid'])
			$validated['tid'] = intval($_POST['tid']);

		#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
		# everything else

		$validated['uid'] = auth_user('uid');
		$validated['tcid'] = intval($_POST['tcid']);
		$validated['name'] = $_POST['name'];
		$validated['description'] = $_POST['description'];

		# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
		# perform db insertion

		DB::insert('data_tags_info', $validated);

	} while (0);

	# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
	# cleanup

	json_exit([
		'err' => isset($err) ? 1 : 0,
		'msg' => $err ?? 'tag ' . ($_POST['tid'] ? 'edited' : 'created') . ' successfully.',
		'redirect' => isset($err) ? '' : '/volunteer/tags/manage',
	]);

}

################################################################################
# prepare and render template

$tag = $_GET['tid']
	? DB::queryFirstRow(
		'SELECT * FROM data_tags_info WHERE tid = %i ORDER BY ver DESC',
		intval($_GET['tid']) )
	: null;

$cats = DB::query(verctrl_mostrecent_qstr('data_tags_cats', 'tcid') . ' AND discarded IS NULL');

if (!$cats)
	header('Location: /volunteer/tags/manage'); # TODO clean this up

template_render([
	'tag'  => $tag,
	'tcid' => $_GET['tcid'],
	'cats' => $cats,
]);
