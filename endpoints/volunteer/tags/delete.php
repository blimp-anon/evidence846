<? #############################################################################
# /endpoints/volunteer/tags/dalete.php
# delete tag category and all subtags, or delete specified tag
# blimp

auth_requireperm(1);

# require either a tcid or a tid, not both
if (!($_GET['tcid'] xor $_GET['tid']))
	die_response_code(400);

################################################################################
# delete tag category

if ($_GET['tcid'])
	DB::insert('data_tags_cats', [
		'tcid' => intval($_GET['tcid']),
		'uid' => auth_user('uid'),
		'title' => '',
		'description' => '',
		'color_fg' => '',
		'color_bg' => '',
		'discarded' => 'deleted via delete endpoint', ]);

################################################################################
# delete selected tags

# category chosen: find all tags with cat
if ($_GET['tcid']) {

	# hit the db
	$tids2del = DB::query(
		verctrl_mostrecent_qstr('data_tags_info', 'tid', 'tcid = %i AND discarded IS NULL'),
		intval($_GET['tcid']) );

	# grab only tids and convert to normal array
	$tids2del = array_values( array_column($tids2del, 'tid') );

}

# find only specified tag
else $tids2del = [ intval($_GET['tid']) ];

# delete all undeleted tags
DB::insert('data_tags_info', array_map(function($tid) {
		return [
			'tid' => $tid,
			'uid' => auth_user('uid'),
			'name' => '',
			'discarded' => 'deleted via delete endpoint',
		];
	}, $tids2del));

################################################################################
# delete all post associations

DB::update(
	'data_tags_assoc',
	['delete_uid' => auth_user('uid')],
	'tid IN %li AND delete_uid IS NULL',
	$tids2del );

################################################################################
# cleanup

header("Location: /volunteer/tags/manage");
