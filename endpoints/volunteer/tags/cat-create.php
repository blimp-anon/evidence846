<? #############################################################################
# /endpoints/volunteer/tags/manage.php
# endpoint to create new tag category
# blimp

################################################################################
# handle post request

if ($_POST) {

	do {

		# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
		# validation

		$validated = [];

		#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
		# colors

		if (!preg_match('/^#[0-9a-fA-F]{6}$/', $_POST['color_fg'])) {
			$err = 'invalid text color';
			break;
		}

		if (!preg_match('/^#[0-9a-fA-F]{6}$/', $_POST['color_bg'])) {
			$err = 'invalid badge color';
			break;
		}

		$validated['color_fg'] = $_POST['color_fg'];
		$validated['color_bg'] = $_POST['color_bg'];

		#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
		# tcid (for updating)

		if ($_POST['tcid'])
			$validated['tcid'] = intval($_POST['tcid']);

		#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
		# the rest of it

		$validated['uid'] = auth_user('uid');
		$validated['title'] = $_POST['title'];
		$validated['description'] = $_POST['description'];

		# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
		# perform db insertion

		DB::insert('data_tags_cats', $validated);

		# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
		# update css

		css_tagcat_updatereplace(
			$validated['tcid'] ?? DB::queryFirstField('SELECT LAST_INSERT_ID()'),
			[ 'bg' => $validated['color_bg'], 'fg' => $validated['color_fg'] ] );

	} while (0);

	# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
	# cleanup

	json_exit([
		'err' => isset($err) ? 1 : 0,
		'msg' => $err ?? 'tag category created successfully.',
		'redirect' => isset($err) ? '' : '/volunteer/tags/manage',
	]);

}

################################################################################
# prepare and render template

# grab tagcat information
$cat = $_GET['tcid']
	? DB::queryFirstRow(
		'SELECT * FROM data_tags_cats WHERE tcid = %i ORDER BY ver DESC',
		intval($_GET['tcid']) )
	: null;

template_render([
	'cat' => $cat,
]);
