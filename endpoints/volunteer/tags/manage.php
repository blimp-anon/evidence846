<? #############################################################################
# /endpoints/volunteer/tags/manage.php
# endpoint to view and manage tags
# blimp

auth_requireperm(1);

$tags = DB::query(verctrl_mostrecent_qstr('data_tags_info', 'tid') . ' AND discarded IS NULL');

$cats = DB::query(verctrl_mostrecent_qstr('data_tags_cats', 'tcid') . ' AND discarded IS NULL');

template_render([
	'tags' => $tags,
	'cats' => $cats,
]);
