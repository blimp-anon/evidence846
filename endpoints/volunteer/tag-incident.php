<? #############################################################################
# /endpoints/volunteer/tag-incident.php
# manage tags on a publicly visible incident
# blimp

auth_requireperm(1);

################################################################################
# initialize

# default to empty array
$_POST['oldtids'] = $_POST['oldtids'] ?? [];
$_POST['newtids'] = $_POST['newtids'] ?? [];

# determine which tids we are adding, and which we are removing
# additions are all new tids that werent already present in old
# deletions are any tids in old that are now not in new
$add = array_values( array_map( 'intval', array_diff($_POST['newtids'], $_POST['oldtids']) ) );
$del = array_values( array_map( 'intval', array_diff($_POST['oldtids'], $_POST['newtids']) ) );

/*json_exit(['add'=>$add, 'addassoc'=>array_map( function($a) {
		return ['uid' => auth_user('uid'), 'tid' => $a, 'feid' => intval($_POST['feid'])];
	}, $add )]);*/

# perform insertions
if ($add)
	DB::insert( 'data_tags_assoc', array_map( function($a) {
			return ['uid' => auth_user('uid'), 'tid' => $a, 'feid' => intval($_POST['feid'])];
		}, $add ) );

# perform deletions
if ($del)
	DB::update( 'data_tags_assoc',
		['delete_uid' => auth_user('uid')],
		"feid = %i AND tid IN %li",
		intval($_POST['feid']), $del);

json_exit([
	'err' => 0,
	'msg' => 'tags updated successfully.',
	'reload' => true,
]);
