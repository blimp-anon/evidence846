<? #############################################################################
# /endpoints/volunteer/delete-report.php
# endpoint to delete a report
# blimp

auth_requireperm(1);

if (!($_GET['reid'] xor $_GET['feid']))
	die_response_code(400);

# delete raw report and associated sources
if ($_GET['reid']) {

	DB::update('data_raw_events', [
			'action_uid' => auth_user()['uid'],
			'action_taken' => 'delete',
		], 'reid = %i', intval($_GET['reid']));

	DB::update('data_raw_sources', [
			'action_uid' => auth_user('uid'),
			'action_taken' => 'delete',
		], 'reid = %i', intval($_GET['reid']));

}

# delete final report and associated tags
else {

	# delete report
	DB::insert('data_final_events', [
		'feid' => intval($_GET['feid']),
		'uid' => auth_user('uid'),
		'title' => '',
		'thumbnail' => '',
		'date' => '1970-01-01',
		'description' => '',
		'loc_city' => '',
		'loc_state' => '',
		'importance' => 0,
		'delete_reason' => 'deleted by delete endpoint',
	]);

	# delete associated tags
	DB::update('data_tags_assoc', ['delete_uid' => auth_user('uid')], 'feid = %i', intval($_GET['feid']));

	# find all sources
	$fsids2del = array_values( array_unique( DB::queryFirstColumn("SELECT fsid FROM data_final_sources WHERE feid = %i", intval($_GET['feid'])) ) );

	# and perform insertions to delete by expanding the sources to delete
	DB::insert('data_final_sources', array_map(function($fsid) {
		return [
			'fsid' => $fsid,
			'feid' => $_GET['feid'],
			'uid' => auth_user('uid'),
			'type' => '',
			'title' => '',
			'value' => '',
			'medium' => '',
			'active' => 'deleted',
		];
	}, $fsids2del));

}

header('Location: /volunteer/list-reports', true, 303);

die();
