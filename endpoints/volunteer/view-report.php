<? #############################################################################
# /endpoints/volunteer/view-report.php
# view an individual report to take action on it
# blimp

auth_requireperm(1);

$report = DB::queryFirstRow('SELECT * FROM data_raw_events WHERE reid = %s', $_GET['reid']);

$sources_mysqli = DB::queryRaw("SELECT * FROM data_raw_sources WHERE reid = %i AND action_uid IS NULL", $_GET['reid']);

template_render([
	'report' => $report,
	'sources_mysqli' => $sources_mysqli,
]);
