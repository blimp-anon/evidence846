<? #############################################################################
# /endpoints/volunteer/view-incidents.php
# portal for authorized volunteers to begin sifting through incidents
# blimp

auth_requireperm(1);

################################################################################
# grab all submitted events

# raw query, we dont know how many rows were fetching!
$mysqli_raw = DB::queryRaw("SELECT * FROM data_raw_events WHERE action_taken IS NULL");

################################################################################
# prepare variables and render template

template_render([
	'query' => $mysqli_raw,
]);
