<? #############################################################################
# /endpoints/contribute/submit-event.php
# allows anons to submit events to the raw database
# for our volunteers to sift through.
# blimp

################################################################################
# handle post request

if ($_POST) {

	do {

		# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
		# process report

		$validated = [];

		#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
		# captcha

		# check if the response received is one of the answers stored earlier
		if (!captcha_validate($_POST['captcha_resp'], $_POST['captcha_ans'])) {
			$err = 'captcha failed, please try again';
			break;
		}

		#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
		# date

		# check for properly formatted dates
		# this should be the default for <input type='date'> but it cant hurt
		if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $_POST['date'])) {
			$err = 'malformed date entered. please use a date of the form yyyy-mm-dd.';
			break;
		}

		# passed validation
		$validated['date'] = $_POST['date'];

		#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
		# thumbnail

		if (isset($_FILES['thumbnail']) && $_FILES['thumbnail']['size']) {

			# get the mime type of uploaded file
			$thumbnail_mime = mime_content_type($_FILES["thumbnail"]["tmp_name"]);

			# abort if we dont have an image
			if (!preg_match('/^image\/(png|jpeg|bmp|gif)$/', $thumbnail_mime)) {
				var_dump($thumbnail_mime);
				$err = 'invalid file uploaded, please select a png, gif, jpg, or bmp image.';
				break;
			}

			# abort if image is too large
			if ($_FILES['thumbnail']['size'] > 10000000) {
				$err = 'image is too large. please keep it under 10MB.';
				break;
			}

			# determine new file name
			$validated['thumbnail'] = uuid4() . '.png';

			# convert and save image
			imagepng(
				imagecreatefromstring(file_get_contents($_FILES['thumbnail']['tmp_name'])),
				$_SERVER['DOCUMENT_ROOT'] . '/uploads/images/' . $validated['thumbnail']
			);

		}

		# no thumbnail
		else $validated['thumbnail'] = null;

		#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
		# others

		$validated['title']       = $_POST['title'];
		$validated['description'] = $_POST['description'] ?? '';
		$validated['loc_city']    = $_POST['loc_city'];
		$validated['loc_state']   = $_POST['loc_state'];

		#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
		# submit report to mysql

		DB::insert('data_raw_events', $validated);

		# grab autoinc primary key
		$reid = DB::insertId();

		# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
		# process sources

		#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
		# preprocessing

		# remove empty sources
		$sources = array_diff($_POST['sources'], ['']);

		#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
		# prepare insert object

		$source_insert = [];

		# fill out the object
		foreach ($sources as $k => $source) {
			$source_insert[] = [
				'reid' => $reid,
				'type' => 'url',
				'value' => $source,
			];
		}

		#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
		# hit the db with our insertion

		if ($source_insert)
			DB::insert('data_raw_sources', $source_insert);

	} while (0);

	# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
	# cleanup

	# if there was an error and a thumbnail was saved...
	# delete the thumbnail
	if (isset($err) && $validated['thumbnail'] && file_exists($_SERVER['DOCUMENT_ROOT'] . '/uploads/images/' . $validated['thumbnail']))
		unlink(realpath($_SERVER['DOCUMENT_ROOT'] . '/uploads/images/' . $validated['thumbnail']));

	# return to calling script

	json_exit([
		'err' => isset($err) ? 1 : 0,
		'msg' => $err ?? 'thank you for your contribution. our team of ' .
			'volunteers will read your submission and incorporate it into ' .
			'the website.',
		'redirect' => '/',
	]);

}

################################################################################
# handle get request

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# write the page

template_render([
	'captcha' => captcha_generate(),
]);
