<? #############################################################################
# /endpoints/login.php
# volunteer login portal
# blimp

################################################################################
# handle post request

if ($_POST) {

	# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
	# process magic link request

	if (array_key_exists('email', $_POST)) {

		# send the email if this user is in our system
		# we dont care about the return value (hide info from attackers)
		$ret = auth_sendmagiclink( $_POST['email'] ); # TODO modify in production

		$msg = $ret ? 'if the email ' . htmlentities($_POST['email']) . ' is linked to a registered user, a login link has been sent to that address.'
			: 'error encountered.';

		json_exit(['msg' => $msg, 'token' => $token]); # TODO remove token in production

	}

}

################################################################################
# handle get request

if ($_GET) {

	# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
	# process authentication request

	if (array_key_exists('token', $_GET)) {

		if (auth_authenticate($_GET['token']))
			$err = 'authentication successful.';

		else
			$err = 'authentication failed.';

	}

}

################################################################################
# render page

template_render([
	'err' => $err,
]);
