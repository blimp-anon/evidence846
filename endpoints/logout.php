<? #############################################################################
# /endpoints/logout.php
# endpoint to log out of session
# blimp

auth_logout();

header('Location: /', true, 303);

die();
